<?php

namespace Drupal\document_preview\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides route responses for the document_preview module.
 */
class DocumentPreviewModalController extends ControllerBase {

  const VIEWER_DOMAIN = 'https://docs.google.com/viewer';

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->currentRequest = $container->get('request_stack')->getCurrentRequest();
    return $instance;
  }

  /**
   * Returns a modal window with iframe.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   A simple renderable array.
   */
  public function modal() : AjaxResponse {
    $url_parameter = $this->currentRequest->query->get('url');
    // @todo check if this variable real url
    $url = Url::fromUri($url_parameter);

    $title = 'Document Preview';

    $content = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['document-preview-modal--content'],
      ],
      'children' => [
        [
          '#type' => 'html_tag',
          '#tag' => 'iframe',
          '#attributes' => [
            'src' => sprintf('%s?url=%s&embedded=true', self::VIEWER_DOMAIN, $url->toString()),
            'width' => '100%',
            'height' => '100%',
          ],
        ],
        [
          '#type' => 'link',
          '#title' => $this->t('Download'),
          '#url' => $url,
          '#attributes' => [
            'class' => 'document-preview--link',
          ],
        ],
      ],
    ];

    $dialog_options = [
      'width' => 900,
      'height' => 600,
      'resizable' => 'true',
      'dialogClass' => 'document-preview-modal',
    ];

    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand($title, $content, $dialog_options));

    return $response;
  }

}
