<?php

namespace Drupal\document_preview\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'document_preview_field' formatter.
 *
 * @FieldFormatter(
 *   id = "document_preview_formatter",
 *   label = @Translation("Document Preview Formatter"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class DocumentPreviewFieldFormatter extends FileFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The file URL generator service.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->fileUrlGenerator = $container->get('file_url_generator');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'view_type' => 'simplebox',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['view_type'] = [
      '#title' => $this->t('View type'),
      '#type' => 'select',
      '#options' => [
        'simplebox' => $this->t('Simplebox'),
        'modal' => $this->t('Modal window'),
      ],
      '#default_value' => $this->getSetting('view_type'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();

    if ($settings['view_type'] == 'simplebox') {
      $summary[] = $this->t('Render document in Simplebox');
    }
    elseif ($settings['view_type'] == 'modal') {
      $summary[] = $this->t('Render document in Modal Window');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $view_type = $this->getSetting('view_type');

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $entity = $this->fieldDefinition->getTargetEntityTypeId();
      $bundle = $this->fieldDefinition->getTargetBundle();
      $field_name = $this->fieldDefinition->getName();
      $field_type = $this->fieldDefinition->getType();
      $file_uri = $file->getFileUri();
      $filename = $file->getFileName();
      $uri_scheme = StreamWrapperManager::getScheme($file_uri);

      if ($uri_scheme == 'public') {
        $url = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
        $elements[$delta] = [
          '#theme' => 'document_preview_field',
          '#url' => $url,
          '#filename' => $filename,
          '#delta' => $delta,
          '#entity' => $entity,
          '#bundle' => $bundle,
          '#field_name' => $field_name,
          '#field_type' => $field_type,
          '#view_type' => $view_type,
          '#attached' => [
            'library' => [
              'document_preview/document-preview',
            ],
          ],
        ];

      }
      else {
        $this->messenger()->addError(
          $this->t('The file (%file) is not publicly accessible. It must be publicly available in order for the Google Docs viewer to be able to access it.',
          ['%file' => $filename]
          ),
          FALSE
        );
      }
    }

    return $elements;
  }

}
